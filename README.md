A prototype of a complete revamp of the application based on a multiple-choice-question kind of game.
So far no database is needed as all the debug data needed is hard coded.

As well as trying new functionality this prototype is an exercise in writing an application more in line with the react way of thinking.
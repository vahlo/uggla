﻿import React, { Component } from 'react';
import { Button, DropdownButton, MenuItem } from 'react-bootstrap';
import { Card } from './Card';
import './Memory.css';


export class Memory extends Component {
    displayName = Memory.name
    CardComponent = Card

    constructor(props) {
        super(props);
        this.state = {
            points: 0,
            memoryCardList: [],
            loading: true,
            groupName: '',
            groupList: []
        };

        this.handleFlippChange = this.handleFlippChange.bind(this);


        fetch('api/Memory/GetCards')
            //.then(res => res.text())          // convert to plain text
            //.then(text => console.log(text))  // then log it out
            .then(response => response.json())
            .then(data => {
                this.setState({
                    memoryCardList: this.shuffleCards(data),
                    loading: false,
                    groupList: data
                        .map(card => card.memoryCardGroup)
                        .filter((val, index, self) => self.indexOf(val) === index)
                });
            });
    }
    handleFlippChange(flippedAlt) {

        var tempCardList = this.state.memoryCardList
        for (var i = 0; i < tempCardList.length; i++) {
            if (tempCardList[i].memoryCardDescription === flippedAlt) {
                tempCardList[i].memoryCardFlipped = !tempCardList[i].memoryCardFlipped;
            }
        }

        this.setState({
            memoryCardList: tempCardList
        });

    }

    /**
    * GENERATEMEMORYGRID returns a grid of memory items, or cards, to
    * be rendered. It takes one argument, groupName, which determines 
    * what cards will populate the grid.
    */
    generateMemoryGrid = (groupName) => {
        var cardList = this.state.memoryCardList;
        var sources;

        if (groupName !== '') {
            sources = cardList.filter(card => card.memoryCardGroup === groupName);
        } else {
            sources = cardList;
        }
        //this.shuffleCards(sources);
        const MyFilteredShuffeledCards = sources;



        return (
            <div className="grid">
                {MyFilteredShuffeledCards.map((card, index) => {
                    return (
                        <Card
                            key={index}
                            card={card}
                            flipped={card.memoryCardFlipped}
                            
                            onFlippChange={this.handleFlippChange}
                        />
                    );
                })}

            </div>
        );


    }

	/**
     * SHUFFLECARDS randomizes list element order in-place.
     * Using Durstenfeld shuffle algorithm.
     */
    shuffleCards(list) {
        var _shuffList = list
        for (var i = _shuffList.length - 1; i > 0; i--) {
            var j = Math.floor(Math.random() * (i + 1));
            var temp = _shuffList[i];
            _shuffList[i] = _shuffList[j];
            _shuffList[j] = temp;
        }
        return _shuffList;
    }

    /**
    * RESETMEMORY repopulates the memory grid by showing the cards that were 
    * potentially hidden by the handleClick function or the group filter.
    */
    resetMemory = () => {
        var elements = document.getElementsByClassName('Card')

        for (var i = 0; i < elements.length; i++) {
            elements[i].style.visibility = 'visible';
        }
        this.setState(() => {
            return {
                groupName: '',
                points: 0
            };
        });
    }

    /**
    * HANDLECLICK is bound to every cards onclick event and logs
    * or displays information about the object, aswell as add extra
    * gameplay functionality.
    */
    handleClick = (source) => {
        //console.log('this is: ', source);




        // document.getElementById(source.memoryCardKey).style.transform = "rotateY(0deg)"



        //document.getElementById(source.memoryCardKey).style.visibility = 'hidden'
        //alert(source.memoryCardDescription)

        document.getElementById(source.memoryCardKey).style.visibility = 'hidden';
        var answer = prompt('Vad är det för djur måntro?');
        if (answer != null && answer.toLocaleUpperCase() === source.memoryCardDescription.toLocaleUpperCase()) {
            alert('Helt rätt!')
            this.setState(() => {
                return { points: this.state.points + 1 };
            });
        } else {
            alert("Naaj! Det var enna " + source.memoryCardDescription + "!")
        }
    }

    /**
     * CLEARGRID is a big no-no for now.
     * Further work needed.
     */
    clearGrid = () => {
        var elements = document.getElementsByClassName('Card')

        for (var i = 0; i < elements.length; i++) {
            elements[i].style.display = 'none';
        }
    }

    /**
     * GENERATEFILTERDROPDOWN returns a dynamically populated
     * dropdown button with the unique group names as menu items.
     */
    generateFilterDropdown = () => {
        var _groupList = this.state.groupList

        return (
            <DropdownButton
                bsStyle='warning'
                title='Card filter'
                id='filter'
            >
                <MenuItem key='All' onSelect={() => this.filterCards('All')}>Show all</MenuItem>
                {_groupList.map((group) => {
                    return (
                        <MenuItem key={group} onSelect={() => this.filterCards(group)}>{group}</MenuItem>
                    )
                })}
            </DropdownButton>
        )
    }

    /**
     * FILTERCARDS is bound to every menu item in the filter dropdown
     * menu's onSelect event and filters the visible cards according 
     * to user interaction.
     */
    filterCards = (selectedGroup) => {
        if (selectedGroup === 'All') {
            this.setState(() => {
                return { groupName: '' };
            });
        } else {
            this.setState(() => {
                return { groupName: selectedGroup };
            });
        }

    }

    //loadData = () => {
    //    var _data = JSON.parse(JSON.stringify(jsonData));
    //    console.log(_data)
    //    return _data;
    //} 

    render() {
        let contents = this.state.loading
            ? <p><em>Loading...</em></p>
            : this.generateMemoryGrid(this.state.groupName)
        return (
            <div className="MemoryBody">

                {this.generateFilterDropdown()}

                <Button className="resetButton" onClick={() => this.resetMemory()} >Reset</Button>
                {/*<button onClick={() => this.clearGrid()}>clear</button>*/}

                <h1>Memory prototype .NET Core 2.1 React</h1>
                
                <p>Poäng: {this.state.points}</p>

                {contents}
               
            </div>
            
        );
    }
}

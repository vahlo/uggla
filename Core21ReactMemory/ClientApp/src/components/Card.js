﻿import React, { Component } from 'react';


export class Card extends Component {
    constructor(props) {
        super(props);
       
        this.handleFlippChange = this.handleFlippChange.bind(this);

    }

    handleFlippChange(e) {
        this.props.onFlippChange(e.target.alt);

    }

    render() {
        var cardClass = 'Card';

        if (!this.props.flipped) {
            cardClass += ' notFlipped';

        }
        else {
            cardClass += ' flipped';
        }
        
        var card = this.props.card;
        

        
        
            return (
                <div>

                    <div >
                        <img
                            className={cardClass}
                            onClick={this.handleFlippChange}
                            src={card.memoryCardUrl}
                            alt={card.memoryCardDescription}
                        />
                    </div>

                </div>

            ); 
        
    }
}
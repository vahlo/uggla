﻿import React, { Component } from 'react';
import './testFlipper.css';


const MemoryCardList = [
    { url: 'https://upload.wikimedia.org/wikipedia/commons/thumb/7/7d/Eagle_Owl_IMG_9203.JPG/1280px-Eagle_Owl_IMG_9203.JPG', name: 'Uggla', id: 1, flipped: false, matched: false },

];
export class TestFlipper extends Component {

    constructor(props) {
        super(props);
        this.state = {
            flipped: false
        };
        this.handleFlippChange = this.handleFlippChange.bind(this);

    }

    handleFlippChange(flipped) {
        var temp = !this.state.flipped
        this.setState({
            flipped: temp
        });
        if (this.state.flipped === true) {
            
        }
        else {
            

        }
    }
    render() {

        return (

            <Card
                id={MemoryCardList[0].id}
                cards={MemoryCardList}
                flipped={this.state.flipped}
                onFlippChange={this.handleFlippChange}
            />

        );
    }
}

class Card extends Component {
    constructor(props) {
        super(props);

        

        this.handleFlippChange = this.handleFlippChange.bind(this);

    }

    handleFlippChange(e) {
        this.props.onFlippChange(e.target.value);
    }


    render() {
        
        var cardClass = 'Card';

        if (!this.props.flipped) {
            cardClass += ' notFlipped';
           
        }
        else  {
            cardClass += ' flipped';
        }
        
        return (
            <div 
                onClick={this.handleFlippChange}
            >
                <img
                    className={cardClass}
                    src={this.props.cards[0].url}
                    alt={this.props.cards[0].name}
                />
            </div>


        );
    }

}
﻿import React, { Component } from 'react';
import { Carousel, Image, Button, ButtonGroup, DropdownButton, MenuItem } from 'react-bootstrap';
import './MultipleChoiceQuestions.css'


class GameSettingsPanel extends Component {
    constructor(props) {
        super(props);

        this.handleSelectedGroupChange = this.handleSelectedGroupChange.bind(this);
    }

    handleSelectedGroupChange(e) {
        this.props.onSelectedGroupChange(e.target.value);
    }

    render() {
        return (
            <div className="settingsPanel">
                <ButtonGroup className="buttonRow">
                    <DropdownButton title="Dropdown" id="bg-justified-dropdown">
                        <MenuItem eventKey="1">Dropdown link</MenuItem>
                        <MenuItem eventKey="2">Dropdown link</MenuItem>
                    </DropdownButton>
                    <Button className="resetButton" key="reset" onClick={() => { }} block >Reset</Button>
                </ButtonGroup>
            </div>
        );
    }
}

class GameCardCarousel extends Component {
    //constructor(props) {
    //    super(props);
    //    //this.handleCurrentItemChange = this.handleCurrentItemChange.bind(this);
    //}

    //handleCurrentItemChange(selectedIndex) {
    //    this.props.onCurrentItemChange(selectedIndex)
    //}

    render() {
        return (
            <div>
                <Carousel
                    className='karusell'
                    direction='next'
                    activeIndex={this.props.index}
                    //onSelect={this.handleCurrentItemChange}
                    controls={false}
                    indicators={false}
                    interval={null}
                    responsive="true"
                >
                    {this.props.items.map((item) => {
                        return (
                            <Carousel.Item responsive="true" key={item.key}>
                                <Image
                                    className='bild'
                                    src={item.url}
                                    alt={item.alt}
                                />

                                <Carousel.Caption>
                                    <h3>For debugging purposes:</h3>
                                    <p>This slide is currently showing {item.alt}</p>
                                </Carousel.Caption>

                            </Carousel.Item>
                        );
                    })}
                </Carousel>
            </div>
        );
    }
}

class GameChoiceButtons extends Component {
    constructor(props) {
        super(props);
        //this.state = {
        //    correctAnswer: this.props.currentItem.alt
        //};

        this.handleAnsweredQuestion = this.handleAnsweredQuestion.bind(this);
    }

    handleAnsweredQuestion(id) {
        this.props.onAnsweredQuestion(id)
    }

    shuffledAnswers() {
        return;
    }

    render() {
        var alternatives = this.props.items.map(item => item.alt)
        return (
            <div className="answerGrid">
                <ButtonGroup className="buttonRow">
                    <Button className="answerButton btn-warning" key={alternatives[0]} onClick={() => this.handleAnsweredQuestion(alternatives[0])} block>{alternatives[0]}</Button>
                    <Button className="answerButton btn-warning" key={alternatives[1]} onClick={() => this.handleAnsweredQuestion(alternatives[1])} block>{alternatives[1]}</Button>
                </ButtonGroup>
                <ButtonGroup className="buttonRow">
                    <Button className="answerButton btn-warning" key={alternatives[2]} onClick={() => this.handleAnsweredQuestion(alternatives[2])} block>{alternatives[2]}</Button>
                    <Button className="answerButton btn-warning" key={alternatives[3]} onClick={() => this.handleAnsweredQuestion(alternatives[3])} block>{alternatives[3]}</Button>
                </ButtonGroup>
            </div>
        );
    };
}

class MultipleChoiceGame extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedGroup: '',
            currentItemIndex: 0,
            groupList: this.props.items
                .map(item => item.group)
                .filter((val, index, self) => self.indexOf(val) === index),
            itemList: this.shuffleCards(this.props.items),
        };

        this.handleSelectedGroupChange = this.handleSelectedGroupChange.bind(this);
        this.handleItemListChange = this.handleItemListChange.bind(this);
        //this.handleCurrentItemChange = this.handleCurrentItemChange.bind(this);
        this.handleAnsweredQuestion = this.handleAnsweredQuestion.bind(this);
    }

    /**
     * SHUFFLECARDS takes a list and returns a shuffled
     * version of it, using Durstenfeld shuffle algorithm.
     * @param list: A list to be shuffled.
     * @returns A shuffled list.
     */
    shuffleCards(list) {
        var _shuffList = list
        for (var i = _shuffList.length - 1; i > 0; i--) {
            var j = Math.floor(Math.random() * (i + 1));
            var temp = _shuffList[i];
            _shuffList[i] = _shuffList[j];
            _shuffList[j] = temp;
        }
        return _shuffList;
    }

    handleSelectedGroupChange(selectedGroup) {
        if (selectedGroup === 'All') {
            this.setState(() => {
                return {
                    groupName: ''
                };
            });
        } else {
            this.setState(() => {
                return {
                    groupName: selectedGroup
                };
            });
        }
    }

    handleItemListChange(itemList) {
        this.setState({
            itemList: itemList
        })
    }

    //handleCurrentItemChange(currentItemIndex) {
    //    this.setState({
    //        currentItemIndex: currentItemIndex
    //    })
    //}

    handleAnsweredQuestion(answer) {
        if (this.state.itemList[this.state.currentItemIndex].alt === answer) {
            console.log("success!")

            let index = this.state.currentItemIndex
            const max = this.state.itemList.length - 1

            index += 1

            if (index > max) {
                // At max index, start from the beginning
                index = 0
            }

            this.setState({
                currentItemIndex: index
            });
            //console.log(this.state.currentItemIndex);
        }
    }

    render() {
        return (
            <div>
                <GameSettingsPanel
                    groupList={this.state.groupList}
                    selectedGroup={this.state.selectedGroup}
                    onSelectedGroupChange={this.handleSelectedGroupChange}

                />

                <GameCardCarousel
                    selectedGroup={this.state.selectedGroup}
                    items={this.state.itemList}
                    index={this.state.currentItemIndex}
                    //onCurrentItemChange={this.handleCurrentItemChange}

                />

                <GameChoiceButtons
                    items={this.state.itemList}
                    currentItem={this.state.itemList[this.state.currentItemIndex].alt}
                    onAnsweredQuestion={this.handleAnsweredQuestion}

                />
            </div>
        );
    }
}


const ITEMS = [
    { url: 'https://wallpapercave.com/wp/owK4MJy.jpg', alt: 'uggla1', group: 'birds', key: 1 },
    { url: 'https://wallpaper.wiki/wp-content/uploads/2017/06/Owl-hd-wallpaper-wide-1024x640.jpg', alt: 'uggla2', group: 'birds', key: 2 },
    { url: 'https://i.pinimg.com/originals/b4/ed/e9/b4ede98748079db08512193afefa7b01.jpg', alt: 'uggla3', group: 'birds', key: 3 },
    { url: 'https://wallpaper.wiki/wp-content/uploads/2017/05/wallpaper.wiki-Cute-Owl-Photo-HD-PIC-WPE007530-1024x640.jpg', alt: 'uggla4', group: 'birds', key: 4 }
];

export class MultipleChoiceQuestions extends Component {
    render() {
        return (
            <div>
                <MultipleChoiceGame items={ITEMS} />
            </div>
        )
    }
}

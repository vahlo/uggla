﻿﻿import React, { Component } from 'react';
import './Memory.css';


const StartMenuList = [
    {
        url: 'https://cdn.shopify.com/s/files/1/1366/6191/products/memory-game-forest-all_large.jpg?v=1515192381'
        , name: 'MemoryGame', id: 1, projectlink: "/Memory"
    },
    {
        url: 'https://static1.squarespace.com/static/56acc1138a65e2a286012c54/t/59dbcc870abd045b7cc4f830/1507576973983/Butler+Image+1.jpg'
        , name: 'OneByOne', id: 2, projectlink: "/MultipleChoiceQuestions"
    },
];


export class StartMenu extends Component {


    render() {
        return (

            <div className="MenuGrid">

                {StartMenuList.map((ImageItem, index) => {
                    return (
                        <Bild
                            key={index}
                            Bilden={ImageItem}
                        />
                    )
                }
                )
                }
            </div>
        );
    }
}

class Bild extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        var bildClass = "Bild"
        document.body.className = "StartMenuBody-Color";


        return (
            <div className="Images-Menu">
                <div>
                    <h1> {this.props.Bilden.name} </h1>
                    <a href={this.props.Bilden.projectlink} class="active">
                        <img
                            className={bildClass}
                            src={this.props.Bilden.url}
                            alt={this.props.Bilden.name}
                        />
                    </a>
                </div>
            </div>

        );


    }
}

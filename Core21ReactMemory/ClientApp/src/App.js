import React, { Component } from 'react';
import { Route } from 'react-router';
import { Layout } from './components/Layout';
import { Home } from './components/Home';
import { FetchData } from './components/FetchData';
import { Counter } from './components/Counter';
import { Memory } from './components/Memory';
import { TestFlipper } from './components/Testflipper';
import { StartMenu } from './components/StartMenu';
import { MultipleChoiceQuestions } from './components/MultipleChoiceQuestions'



export default class App extends Component {
  displayName = App.name

  render() {
    return (
        <Layout>
            <Route path='/' component={StartMenu} />
        <Route path='/counter' component={Counter} />
        <Route path='/fetchdata' component={FetchData} />   
        <Route path='/memory' component={Memory} />  
            <Route path='/testFlipper' component={TestFlipper} />

            <Route path='/MultipleChoiceQuestions' component={MultipleChoiceQuestions} />


      </Layout>
    );
  }
}

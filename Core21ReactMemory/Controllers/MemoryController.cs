﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using Newtonsoft.Json;

namespace Core21ReactMemory.Controllers
{
    [Route("api/[controller]")]
    public class MemoryController : Controller
    {
        readonly IConfiguration _config;
        public MemoryController(IConfiguration config)
        {
            _config = config;
        }

     
        [HttpGet("[action]")]
        public List<MemoryCard> GetAnimals()
        {
            List<MemoryCard> items = new List<MemoryCard>();
            using (StreamReader r = new StreamReader("./ClientApp/src/data/animals.json"))
            {
                string json = r.ReadToEnd();
                items = JsonConvert.DeserializeObject<List<MemoryCard>>(json);
            }

            return items;
        }

        [HttpGet("[action]")]
        public List<MemoryCard> GetMemoryItems()
        {
            string conString = _config.GetSection("Config").GetSection("ConnString").Value;

            List<MemoryCard> memoryCards = new List<MemoryCard>();

            SqlConnection conn = new SqlConnection(conString);
            conn.Open();

            using (conn)
            {
                SqlCommand cmd = new SqlCommand();
                cmd = new SqlCommand("sp_GetAllMemoryItems", conn)
                {
                    CommandType = CommandType.StoredProcedure
                };
                //cmd.Parameters.Add("@GroupName", SqlDbType.VarChar).Value = GroupName;
                
                SqlDataReader reader = null;
                reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    MemoryCard m = new MemoryCard
                    {
                        MemoryCardKey = Convert.ToInt32(reader["fld_MemoryID"].ToString()),
                        MemoryCardDescription = reader["fld_Alt"].ToString(),
                        MemoryCardUrl = reader["fld_SourceURL"].ToString(),
                        MemoryCardGroup = reader["fld_GroupName"].ToString(),
                        
                    };
                    memoryCards.Add(m);

                }
                conn.Close();
                return memoryCards;
            }
        }
        [HttpGet("[action]")]
        public List<MemoryCard> GetCards()
        {
            List<MemoryCard> memoryCards = new List<MemoryCard>();
            MemoryCard m1 = new MemoryCard
            {
                MemoryCardKey = 1,
                MemoryCardUrl = "https://images.unsplash.com/photo-1534695941753-73cf13435eb4?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=51da81e3eeb6a029188a6dfd1eea6652&auto=format&fit=crop&w=1868&q=80",
                MemoryCardDescription = "Muckarduva"
            };

            MemoryCard m2 = new MemoryCard
            {
                MemoryCardKey = 2,
                MemoryCardUrl = "https://upload.wikimedia.org/wikipedia/commons/thumb/7/7d/Eagle_Owl_IMG_9203.JPG/1280px-Eagle_Owl_IMG_9203.JPG",
                MemoryCardDescription = "Ugglebest"
            };

            MemoryCard m3 = new MemoryCard
            {
                MemoryCardKey = 3,
                MemoryCardUrl = "https://d1ia71hq4oe7pn.cloudfront.net/photo/67454991-480px.jpg",
                MemoryCardDescription = "Stare"
            };

            memoryCards.Add(m1);
            memoryCards.Add(m2);
            memoryCards.Add(m3);

            return memoryCards;
        }


        public class MemoryCard
        {
            public int MemoryCardKey { get; set; }
            public string MemoryCardUrl { get; set; }
            public string MemoryCardDescription { get; set; }
            public string MemoryCardGroup { get; set; }
            public bool MemoryCardFlippedMyProperty { get; set; }
        }
    }
}

